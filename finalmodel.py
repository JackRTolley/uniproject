import torch
import torch.nn as nn
import torch.nn.functional as F
import pickle
import os.path
from copy import deepcopy
import argparse

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import os
import time
import numpy as np
import pandas as pd
import seaborn as sns

from torch.utils.data import TensorDataset, DataLoader

from sklearn import preprocessing

import argparse 

def report(epoch, total_loss):
    if epoch % 100 == 0:
        print("Epoch {} | Loss {}".format(epoch, total_loss))

class MNet(nn.Module):

    '''
    A class that represents a Linear Feed Forward Model, with a arbitary amount
    of nodes and hidden layers
    '''
    def __init__(self, name: str = None, dataset:str = None, optimizer:str=None):
        super(MNet, self).__init__()
        # Saving Parameters
        if name is None:
            self.name = "relu-{}-{}".format("happy", "goluch")
        else:
            self.name = name
        
        self.dataset = dataset

        # Required Parameters
        self.epochs = 0
        self.best_loss = 1000000000
        self.optimizer = optimizer

        # Assign Activation function
        
        self.model = nn.Sequential(
            nn.Linear(5, 290),
            nn.Tanh(),
            nn.Linear(290,1)
        )

    def forward(self, x):
        return self.model(x)

    def save_model(self, optimizer):
        path = 'models/{0}.ckpt'.format(self.name)
        #print("Saving Model - {0}".format(self.name))
        torch.save({
            'name':self.name,
            'epoch':self.epochs,
            'dataset':self.dataset,
            'best_loss':self.best_loss,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
        }, path)

    def load_model(self, eval=False, optimizer=None):
        path = 'models/{0}.ckpt'.format(self.name)
        if os.path.isfile(path):
            print("Loading {0}".format(self.name))
            checkpoint = torch.load(path)
            self.name = checkpoint['name']
            self.dataset = checkpoint['dataset']
            self.epochs = checkpoint['epoch']
            self.best_loss = checkpoint['best_loss']
            self.model.load_state_dict(checkpoint['model_state_dict'])
            if not eval and optimizer is not None:
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                return True      
        else:
            print("No Model To Load")
            return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('type', help='Model Type')
    args= parser.parse_args()

    # Load data 
    data = np.loadtxt('data/training{}.csv'.format(args.type), delimiter=",", skiprows=1)
    data_x = data[:, 1:6]
    data_y = data[:, 6]
    data_y = np.reshape(data_y,(-1,1))

    data_x = torch.Tensor(data_x)
    data_y = torch.Tensor(data_y)

    dataset = TensorDataset(data_x, data_y)
    dataset = DataLoader(dataset, num_workers=0, batch_size=8)
    # Load stopping dataset
    early = np.loadtxt('data/monitor{}.csv'.format(args.type), delimiter=",", skiprows=1)
    early_x = early[:, 1:6]
    early_y = early[:, 6]
    early_y = np.reshape(early_y,(-1,1))

    early_x = torch.Tensor(early_x)
    early_y = torch.Tensor(early_y)

    earlyset = TensorDataset(early_x, early_y)
    earlyset = DataLoader(earlyset, num_workers=0, batch_size=64)

    # Create net
    model = MNet(
        name='Final{}'.format(args.type),
        dataset='training{}'.format(args.type)
    )
    #torch.save(model, "models/Final{}.pt".format(args.type))

    #model = torch.load('models/Final{}.pt'.format(args.type))
    #model.load_model()

    optimizer = optim.Adam(model.parameters(), weight_decay=0.01)
    #model.load_model(optimizer=optimizer)
    criterion = nn.MSELoss()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)



    # Train
    min_val_loss = 10000000000
    start_epoch = model.epochs
    for epoch in range(25001):
        total_loss = 0
        model.train()
        for data, labels in dataset:   
            data = data.to(device)
            labels = labels.to(device)
            # Training loop
            y_pred = model(data)
            loss = criterion(y_pred, labels)
            optimizer.zero_grad()    
            loss.backward()
            optimizer.step()
            # Save loss of batch
            total_loss += loss.item()
        if epoch % 100 == 0:
            val_loss = 0
            model.eval()
            with torch.no_grad():
                for data, labels in earlyset:
                    data = data.to(device)
                    labels = labels.to(device)
                    y_pred = model(data)
                    loss = criterion(y_pred, labels)
                    val_loss += loss.item()

            if val_loss < min_val_loss:
                min_val_loss = val_loss
                model.best_loss = val_loss
                model.epochs = start_epoch + epoch
                model.save_model(optimizer)
                torch.save(model, "models/Final{}.pt".format(args.type))

            report(epoch, total_loss)
import argparse

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import os
import time
import numpy as np
import pandas as pd
import seaborn as sns

from torch.utils.data import TensorDataset, DataLoader

from sklearn import preprocessing
from finalmodel import MNet

if __name__ == "__main__":

    torch.nn.Module.dump_patches = True

    parser = argparse.ArgumentParser()
    parser.add_argument('name', help='Name of model')
    parser.add_argument('dataset', help='Dataset to test against')
    args = parser.parse_args()

    # Validate arguments
    exists = os.path.isfile('models/{}.pt'.format(args.name))
    if not exists:
        raise ValueError("Model does not exist")
    exists = os.path.isfile('data/{}.csv'.format(args.dataset))
    if not exists:
        raise ValueError("Dataset does not exist")
    # Load Model
    model = torch.load('models/{}.pt'.format(args.name))
    model.to(torch.device('cpu'))
    #model.load_model(eval=True)
    # Load Dataset
    data = np.loadtxt('data/{}.csv'.format(args.dataset), delimiter=",", skiprows=1)
    data_x = data[:, 1:6]
    data_y = data[:, 6]
    data_y = np.reshape(data_y,(-1,1))

    data_x = torch.Tensor(data_x)
    data_y = torch.Tensor(data_y)

    dataset = TensorDataset(data_x, data_y)
    dataset = DataLoader(dataset, num_workers=0, batch_size=10)
    # Evaluate
    print("Evaluation Started")
    results = []
    model.eval()
    with torch.no_grad():
        for data, labels in dataset:
            y_pred = model(data)
            labels = labels.detach().numpy().tolist()
            y_pred = y_pred.detach().numpy().tolist()
            results.append( (labels, y_pred) )
    # Process results
    new_results = []
    for x in results:
        new_results.extend(list(zip(x[0], x[1])))
    results = new_results
    results =  [(x[0][0], x[1][0] ) for x in results ]
    # Evaluate
    err = [(x[1]-x[0])/x[0] for x in results]
    abs_err = [abs(x) for x in err]
    avg_err = sum(abs_err)/len(abs_err)
    med_err = sorted(abs_err)[len(abs_err)//2]

    square_err = [(x[0] - x[1])**2 for x in results]
    sq_sum = sum(square_err)
    MSE = (1/ (2*len(results))) * sq_sum
    std = np.std(np.array(abs_err))

    print("Training Time: {}".format(model.epochs))
    print("Average Error: {}".format(avg_err))
    print("Median Error: {}".format(med_err))
    print("MSE: {}".format(MSE))
    print("STD: {}".format(std))
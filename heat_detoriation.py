from math import pi

"""
Author: Jack Tolley
Email: jt1194@york.ac.uk
"""

# https://www.sciencedirect.com/science/article/pii/S0029549306004584

def Yamagata_1972(heat_flux: list, mass_flux: list) -> list:
    # Simple correlation for heat flux
    # Returns True if data is unaffected by heat detoration
    result = [] 
    for i in mass_flux.index:
        G = mass_flux[i]
        q = 200*(G**1.2)
        if heat_flux[i]*1000 > q:
            result.append(False)
        else:
            result.append(True)
    return result

def Styrikovich_1967(heat_flux:list, mass_flux:list) -> list:

    # Simple correlation for heat flux
    # Returns True if data is unaffected by heat detoration
    
    result = [] 
    for i in mass_flux.index:
        G = mass_flux[i]
        q = 580*G
        if heat_flux[i]*1000 > q:
            result.append(False)
        else:
            result.append(True)
    return result

# TODO Jackson and Hall Criterion(1979)

# TODO Ogata and Sato(1972)

# TODO Petuhkov and Kurganov (1983)


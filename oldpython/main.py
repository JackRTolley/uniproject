import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split

import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn import preprocessing

from model import Net
from evaluator import Evaluator

if __name__ == "__main__":

    if torch.cuda.is_available():
        print("Using CUDA")
        device = torch.device("cuda")
    else:
        print("Using CPU")
        device = torch.device("cpu")

    
    # Load Data
    train_data=np.loadtxt("data/training_data.csv", delimiter=",", skiprows=1)
    test_data =np.loadtxt("data/testing_data.csv", delimiter=",", skiprows=1)
    train_x=train_data[:, 1:6]
    train_y=train_data[:,6]
    test_x=test_data[:, 1:6]
    test_y=test_data[:, 6]

    # Reshape Y's
    train_y=np.reshape(train_y,(-1,1))
    test_y =np.reshape(test_y,(-1,1))

    # Create Datasets
    train_x = torch.Tensor(train_x)
    train_y  = torch.Tensor(train_y)
    test_x = torch.Tensor(test_x)
    test_y = torch.Tensor(test_y)
    train_dataset = TensorDataset(train_x, train_y)
    test_dataset  = TensorDataset(test_x, test_y)

    train = DataLoader(train_dataset, num_workers=0, batch_size=128)
    test  = DataLoader(test_dataset, num_workers=0, batch_size=128)

    results_dict = {}

    for layers in [2,4]:
        for nodes in [10,50,100,250]:
            start_time = time.time()
            print("------ Working {} {} ------".format(layers, nodes))
            model = Net(layers, nodes)
            model = model.to(device)
            criterion = torch.nn.MSELoss()

            optimizer = torch.optim.Adam(model.parameters())
            model.load_model(eval=False, optimizer=optimizer)
            # Training Loop
            improved = False
            model.train()
            for epoch in range(9000):
                total_loss = 0
                for data, labels in train: 
                    data = data.to(device)
                    labels = labels.to(device)
                    y_pred = model(data)
                    loss = criterion(y_pred, labels)
                    total_loss += loss.item()
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()
                if total_loss < model.best_loss:
                    #print("New Best Model")
                    improved = True
                    model.save_model(optimizer)
                    model.best_loss = total_loss
                if epoch%100== 0:
                    print("Epoch {} | Loss {} | Improved? {}".format(epoch, total_loss, improved))
                    improved = False

            netEvaluator = Evaluator(train, test, device)
            netEvaluator.set_net(model)

            # * All of our graphing evaluations
            #netEvaluator.graph_predicted()
            #netEvaluator.graph_predicted(test=False)
            #netEvaluator.graph_error()
            #netEvaluator.graph_error(test=False)
            #netEvaluator.graph_best_worst(amount=10)
            #netEvaluator.graph_best_worst(amount=50)
            #netEvaluator.graph_best_worst(amount=100)
            #netEvaluator.graph_fig1()
            #netEvaluator.graph_fig2()

            # * All of our evaluation functions
            test_err = netEvaluator.mean_abs_error()
            train_err = netEvaluator.mean_abs_error(test=False)
            avg_fig1, med_fig1 = netEvaluator.eval_fig1()
            avg_fig2, med_fig2 = netEvaluator.eval_fig2()

            #netEvaluator.best_performers()
            #netEvaluator.worst_performers()
            # * Report Functionality
            results_dict["{}-{}".format(layers,nodes)] = (test_err, train_err)
            print("Finished: ", time.time()-start_time)
            data = pd.read_csv('data/fig_eval.csv', index_col=0)
            the_result = pd.DataFrame(data=[[test_err, train_err, avg_fig1, med_fig1, avg_fig2, med_fig2]], index=[model.name], 
             columns=['test_err', 'train_err', 'avg_fig1', 'med_fig1','avg_fig2', 'med_fig2'])
            data = data.append(the_result)
            data.to_csv('data/fig_eval.csv')

    # ! 8 Layers, 100 Neurons is the best
    print(results_dict)
    print("Finished")
    

import torch
import torch.nn as nn
import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from torch.utils.data import TensorDataset, DataLoader

class Evaluator:
    def __init__(self, train_dataset, test_dataset, device):
        self.net = None
        self.train_dataset = train_dataset
        self.test_dataset = test_dataset
        self.device = device
        self.test_results = None
        self.train_results = None
    
    def set_net(self, net, load=True): 
        self.net = net
        self.net.load_model(eval=True)
        self.net = self.net.to(self.device)
        self.train_results = self.test(self.train_dataset)
        self.test_results = self.test(self.test_dataset)

    def test(self, dataset):

        print("----- Evaluating -----") 
        self.net.eval()
        results = []
        with torch.no_grad():
            for data, labels in dataset:
                # Compute on CUDA
                data = data.to(self.device)
                #labels = labels.to(self.device)
                predictions = self.net(data)

                # Bring Back to CPU to store
                data = data.cpu().numpy().tolist()
                labels = labels.numpy().tolist()
                predictions = predictions.cpu().numpy().tolist()
                results.extend( [(*data[x], labels[x][0], predictions[x][0])
                                    for x in range(len(data))])
        
        results = pd.DataFrame(results, columns=[
            'Mass Flux',
            'Heat Flux',
            'Pressure',
            'Diameter',
            'Bulk Enthalpy',
            'Real Wall Temp',
            'Pred Wall Temp'
        ])

        results["Error"] = (results['Pred Wall Temp'] - 
                            results['Real Wall Temp'])/results['Real Wall Temp']

        results['Abs Error'] = [abs(x) for x in results['Error'].tolist()]

        return results

    def graph_predicted(self, test=True):
        
        if test:
            title = 'test'
            data = self.test_results
        else:
            title = 'train'
            data = self.train_results

        plot = sns.lmplot(x='Real Wall Temp',
                          y='Pred Wall Temp',
                          data=data)
        plt.plot([350,500],[350,500], linewidth=2, color="green")
        plot.savefig('figures/{}_actual_predicted'.format(title))
        plt.show()

    def graph_error(self, test=True):
        if test:
            title = 'test'
            data = self.test_results
        else:
            title = 'train'
            data = self.train_results
    
        plot = sns.lmplot(x='Real Wall Temp', y='Error', data=data)
        plt.plot([350,500],[0,0], linewidth=2, color="green")
        plot.savefig('figures/{}_error'.format(title))
        plt.show()

    def mean_abs_error(self, test=True):
        if test:
            data = self.test_results
        else:
            data = self.train_results
        
        abs_error = sum(data['Abs Error'])/len(data)

        return abs_error

    def best_performers(self, test=True, amount=10):
        if test:
            data = self.test_results
        else:
            data = self.train_results

        data = data.sort_values('Abs Error')

        return data.iloc[:amount]

    def worst_performers(self, test=True, amount=10):
        if test:
            data = self.test_results
        else:
            data = self.train_results

        data = data.sort_values('Abs Error', ascending=False)
        return data.iloc[:amount]

    def graph_best_worst(self, test=True, amount=10):
        if test:
            title = 'test'
        else:
            title = 'train'

        best  = self.best_performers(test, amount)
        worst = self.worst_performers(test, amount)

        best['Type'] = 'Best'
        worst['Type'] = 'Worst'

        results = best.append(worst)
        #results  = best
        results = results.drop(['Error', 'Abs Error'], axis=1)
        plot = sns.pairplot(results, hue='Type')
        plot.savefig('figures/{}_best_worst_{}'.format(title, amount))
        plt.show()

    def graph_test(self, params):
        data = pd.read_csv('data/raw_data.csv', header=1)
        
        data = data.loc[:,[
            "Mass Flux", "Heat Flux", "Pressure", "Diameter",
            "Bulk Enthalpy" , "Wall Temperature"
        ]]
        # Get Data 
        data = data.loc[data['Mass Flux']== params['Mass Flux']]
        data = data.loc[data['Heat Flux']== params['Heat Flux']]
        data = data.loc[data['Pressure'] == params['Pressure']]
        data = data.loc[data['Diameter'] == params['Diameter']]
        data = self.normalize_data(data)
        # Make Predictions

        graph_x = data.iloc[:,0:5].values
        graph_y = data.iloc[:,5].values
        graph_y = np.reshape(graph_y, (-1,1))
        graph_x = torch.Tensor(graph_x)
        graph_y = torch.Tensor(graph_y)

        graph_dataset = TensorDataset(graph_x, graph_y)
        graph = DataLoader(graph_dataset, num_workers=0, batch_size=32)

        results = []

        with torch.no_grad():
            for graph_data, labels in graph:
                graph_data = graph_data.to(self.device)
                result = self.net(graph_data)
                results.extend(result.cpu().numpy().tolist())
        
        results = pd.DataFrame(results, columns=['Wall Temperature'])
        results['Bulk Enthalpy'] = data['Bulk Enthalpy'].values
        results['Type'] = 'Predicted'
        data['Type'] = 'Actual'
        data = data.append(results)
        #print(data)
        # Graph
        plot = sns.scatterplot(x='Bulk Enthalpy', y='Wall Temperature',
        hue='Type', data=data)
        plot = plot.get_figure()
        plot.savefig('figures/'+params['Name'])
        plt.show()

    def eval_figures(self, params):
        data = pd.read_csv('data/raw_data.csv', header=1)
        
        data = data.loc[:,[
            "Mass Flux", "Heat Flux", "Pressure", "Diameter",
            "Bulk Enthalpy" , "Wall Temperature"
        ]]
        # Get Data 
        data = data.loc[data['Mass Flux']== params['Mass Flux']]
        data = data.loc[data['Heat Flux']== params['Heat Flux']]
        data = data.loc[data['Pressure'] == params['Pressure']]
        data = data.loc[data['Diameter'] == params['Diameter']]
        data = self.normalize_data(data)
        # Make Predictions
        graph_x = data.iloc[:,0:5].values
        graph_y = data.iloc[:,5].values
        graph_y = np.reshape(graph_y, (-1,1))
        graph_x = torch.Tensor(graph_x)
        graph_y = torch.Tensor(graph_y)

        graph_dataset = TensorDataset(graph_x, graph_y)
        graph = DataLoader(graph_dataset, num_workers=0, batch_size=32)

        results = []

        with torch.no_grad():
            for graph_data, labels in graph:
                graph_data = graph_data.to(self.device)
                result = self.net(graph_data)
                result = result.cpu().numpy().tolist()
                labels = labels.cpu().numpy().tolist()
                result = [(result[x][0], labels[x][0]) for x in range(len(result))]
                results.extend(result)
        abs_acc = [ abs((x[0]-x[1])/x[1]) for x in results]
        
        avg_acc = sum(abs_acc)/len(abs_acc)
        med_acc = sorted(abs_acc)[len(abs_acc)//2]

        return avg_acc, med_acc


    def graph_fig1(self):
        params = {
            'Name'      : 'graph_fig1',
            'Mass Flux' : 2149.26,
            'Heat Flux' : 787.5, 
            'Pressure' : 22.75,
            'Diameter' : 9.42,
        }
        self.graph_test(params)

    def graph_fig2(self):
        params = {
            'Name'      : 'graph_fig2',
            'Mass Flux' : 700,
            'Heat Flux' : 500, 
            'Pressure' : 22.5,
            'Diameter' : 10,
        }
        self.graph_test(params)

    def eval_fig1(self):
        params = {
            'Name'      : 'graph_fig1',
            'Mass Flux' : 2149.26,
            'Heat Flux' : 787.5, 
            'Pressure' : 22.75,
            'Diameter' : 9.42,
        }
        return self.eval_figures(params)
    
    def eval_fig2(self):
        params = {
            'Name'      : 'graph_fig2',
            'Mass Flux' : 700,
            'Heat Flux' : 500, 
            'Pressure' : 22.5,
            'Diameter' : 10,
        }
        return self.eval_figures(params)

    def normalize_data(self, data):
        normal_wall = data['Wall Temperature']
        mean = pd.Series.from_csv('data/normalize_mean.csv')
        std  = pd.Series.from_csv('data/normalize_std.csv')
        data = (data-mean)/std
        data['Wall Temperature'] = normal_wall
        return data
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, random_split

import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn import preprocessing

from model import Net
from combomodel import ComboNet
from evaluator import Evaluator

if __name__ == "__main__":

    # Load Data
    train_data=np.loadtxt("data/training_data.csv", delimiter=",", skiprows=1)
    test_data =np.loadtxt("data/testing_data.csv", delimiter=",", skiprows=1)
    train_x=train_data[:, 1:6]
    train_y=train_data[:,6]
    test_x=test_data[:, 1:6]
    test_y=test_data[:, 6]

    # Reshape Y's
    train_y=np.reshape(train_y,(-1,1))
    test_y =np.reshape(test_y,(-1,1))

    # Create Tensors
    train_x = torch.Tensor(train_x)
    train_y  = torch.Tensor(train_y)
    test_x = torch.Tensor(test_x)
    test_y = torch.Tensor(test_y)
    
    # classifier
    classifier = torch.load('models/supercritical_classifier.pt')
    # Split into supercritical and subcritical
    train_x = train_x.detach()
    mask = classifier(train_x).detach()
    mask = torch.round(mask).byte()
    test_mask = classifier(test_x).detach()
    test_mask = torch.round(test_mask).byte()
    train_supcritical_x = torch.masked_select(train_x, mask).view(-1,5)
    train_supcritical_y = torch.masked_select(train_y, mask).view(-1,1)
    test_supcritical_x = torch.masked_select(test_x, test_mask).view(-1,5)
    test_supcritical_y = torch.masked_select(test_y, test_mask).view(-1,1)
    mask = mask ^ 1
    test_mask = test_mask ^ 1
    train_subcritical_x = torch.masked_select(train_x, mask).view(-1,5)
    train_subcritical_y = torch.masked_select(train_y, mask).view(-1,1)
    test_subcritical_x = torch.masked_select(test_x, test_mask).view(-1,5)
    test_subcritical_y = torch.masked_select(test_y, test_mask).view(-1,1)
    # Datasets
    train_sup_dataset = TensorDataset(train_supcritical_x, train_supcritical_y)
    test_sup_dataset = TensorDataset(test_supcritical_x, test_supcritical_y)
    train_sub_dataset = TensorDataset(train_subcritical_x, train_subcritical_y)
    test_sub_dataset  = TensorDataset(test_subcritical_x, test_subcritical_y)
    # DataLoaders
    train_sup = DataLoader(train_sup_dataset, num_workers=0, batch_size=128)
    test_sup  = DataLoader(test_sup_dataset , num_workers=0, batch_size=128)
    train_sub = DataLoader(train_sub_dataset, num_workers=0, batch_size=128)
    test_sub  = DataLoader(test_sub_dataset , num_workers=0, batch_size=128)
    
    subcritical = Net(4, 100, name='SubcriticalNet')
    supercritical = Net(4, 100, name='SupercriticalNet')
    
    criterion = nn.MSELoss()
    suboptimizer = optim.Adam(subcritical.parameters())
    superoptimizer = optim.Adam(supercritical.parameters())

    subcritical.train()
    for epoch in range(100):
        total_loss = 0
        for data, labels in train_sub:
            y_pred = subcritical(data)
            loss = criterion(y_pred, labels)
            total_loss += loss.item()
            suboptimizer.zero_grad()
            loss.backward()
            suboptimizer.step()
        if epoch % 100 == 0:
            print("Epoch {} | Loss {}".format(epoch, total_loss))
        if total_loss < subcritical.best_loss:
            subcritical.save_model(suboptimizer)
    supercritical.train()
    for epoch in range(100):
        total_loss = 0
        for data, labels in train_sup:
            y_pred = supercritical(data)
            loss = criterion(y_pred, labels)
            total_loss += loss.item()
            superoptimizer.zero_grad()
            loss.backward()
            superoptimizer.step()
        if epoch % 100 == 0:
            print("Epoch {} | Loss {}".format(epoch, total_loss))
        if total_loss , supercritical.best_loss:
            superoptimizer.save_model(superoptimizer)

    # Evaluate
    subcritical.eval()
    supercritical.eval()

    sub_results = []
    sup_results = []

    for data, labels in test_sub:
        y_pred = subcritical(data)
        sub_results.extend([(labels[x].item(), y_pred[x].item()) 
            for x in range(len(data))])

    for data, labels in test_sup:
        y_pred = supercritical(data)
        sup_results.extend([(labels[x].item(), y_pred[x].item()) 
            for x in range(len(data))])

    results = sub_results + sup_results

    abs_err = [ abs((x[1]-x[0])/x[0]) for x in results]
    print(sum(abs_err)/len(abs_err))

    
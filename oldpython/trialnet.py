import torch
import torch.nn as nn
import torch.nn.functional as F
import os.path

class TrailNet(nn.Module):

    def __init__(self):
        super(TrailNet, self).__init__()
        self.epochs = 0
        self.name   = 'trailnet'
        self.min_loss = 1000000000
        self.fc1 = nn.Linear(5, 1000)
        self.fc2 = nn.Linear(1000, 1000)
        self.fc3 = nn.Linear(1000, 1000)
        self.fc4 = nn.Linear(1000, 1)
        
    def forward(self, x):
        out = F.elu(self.fc1(x))
        out = F.elu(self.fc2(out))
        out = F.elu(self.fc3(out))
        out = self.fc4(out)
        return out

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import TensorDataset, DataLoader, random_split

import pandas as pd 
import numpy as np

from collections import Counter
# Constants
CRITICAL_TEMPERATURE = 373
CRITICAL_PRESSURE = (22-22.875527797326026)/1.524997451874449

if __name__ == '__main__':

    # Load data
    train_data=np.loadtxt("data/training_data.csv", delimiter=",", skiprows=1)
    test_data =np.loadtxt("data/testing_data.csv", delimiter=",", skiprows=1)
    train_x=train_data[:, 1:6]
    train_data = train_data.tolist()
    train_y = np.array([1 
    if (x[6]> CRITICAL_TEMPERATURE and x[2]>CRITICAL_PRESSURE)
    else 0 for x in train_data ])
    test_x=test_data[:, 1:6]
    test_data = test_data.tolist()
    test_y = np.array([1 
    if (x[6]> CRITICAL_TEMPERATURE and x[2]>CRITICAL_PRESSURE)
    else 0 for x in test_data])

    # Reshape Y's
    train_y=np.reshape(train_y,(-1,1))
    test_y =np.reshape(test_y,(-1,1))

    # Create Datasets
    train_x = torch.Tensor(train_x)
    train_y  = torch.Tensor(train_y)
    test_x = torch.Tensor(test_x)
    test_y = torch.Tensor(test_y)
    train_dataset = TensorDataset(train_x, train_y)
    test_dataset  = TensorDataset(test_x, test_y)

    train = DataLoader(train_dataset, num_workers=0, batch_size=128)
    test  = DataLoader(test_dataset, num_workers=0, batch_size=128)

    model = nn.Sequential(
        nn.Linear(5,50),
        nn.ReLU(),
        nn.Dropout(),
        nn.Linear(50,50),
        nn.ReLU(),
        nn.Dropout(),
        nn.Linear(50,1),
        nn.Sigmoid()
    )

    criterion = nn.BCELoss()
    optimizer = optim.Adam(model.parameters())
    
    model.train()
    improved = False
    for epoch in range(2000):
        total_loss = 0
        for data, labels in train: 
            y_pred = model(data)
            loss = criterion(y_pred, labels)
            total_loss += loss.item()
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        if epoch%100== 0:
            print("Epoch {} | Loss {} ".format(epoch, total_loss))


    results = []
    with torch.no_grad():
        for data, labels in test:
            predictions = model(data).numpy().tolist()
            labels = labels.numpy().tolist()
            results.extend([(labels[x][0], predictions[x][0]) for x
            in range(len(data))])

    correct = 0
    incorrect = 0
    mlabels = [x[0] for x in results]
    print(str(Counter(mlabels)))
    for x in results:
        if x[1] > 0.5:
            if x[0] == 1:
                correct += 1
            else:
                incorrect += 1
        else:
            if x[0] == 0:
                correct += 1
            else:
                incorrect += 1
    
    print("Correct: {} | Incorrect: {} | Percentage: {:.2f}".format(
        correct, incorrect, (correct/(incorrect+correct))*100
    ))

    torch.save(model,'models/supercritical_classifier.pt')
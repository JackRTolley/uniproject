import torch

import pandas as pd

if __name__ == "__main__":

    # Load Classifier
    model = torch.load('models/supercritical_classifier.pt')

    # Load Data
    data = pd.read_csv('data/final_data.csv', index_col=0)
    # Classify
    
    mass_flux = data['Mass Flux'].tolist()
    heat_flux = data['Heat Flux'].tolist()
    pressure  = data['Pressure'].tolist()
    diameter  = data['Diameter'].tolist()
    bulk      = data['Bulk Enthalpy'].tolist()

    inputs = [[
        mass_flux[x],
        heat_flux[x],
        pressure[x],
        diameter[x],
        bulk[x]]
        for x in range(len(mass_flux))
    ]
    inputs = torch.Tensor(inputs)
    outputs = model(inputs)
    outputs = [round(x.item()) for x in outputs]

    
    data['type'] = outputs

    sub = data.loc[data['type'] == 0].drop(labels=['type'], axis=1)
    sub_train = sub.sample(frac=0.8)
    sub_test  = pd.concat([sub_train, sub]).drop_duplicates(keep=False)
    sub_test  = sub_test.sample(frac=1)
    sub_train.to_csv('data/sub_train.csv')
    sub_test.to_csv('data/sub_test.csv')

    superc = data.loc[data['type'] == 1].drop(labels=['type'], axis=1)
    super_train = superc.sample(frac=0.8)
    super_test  = pd.concat([super_train, superc]).drop_duplicates(keep=False)
    super_test  = super_test.sample(frac=1)
    super_train.to_csv('data/super_train.csv')
    super_test.to_csv('data/super_test.csv') 


    print('Finished')
import torch
import torch.nn as nn
import torch.nn.functional as F
import pickle
import os.path

class ComboNet(nn.Module):


    def __init__(self, subcritical_net, supercritical_net, classifier):
        super(ComboNet, self).__init__()
        # Set Parameters
        self.subnet = subcritical_net
        self.supernet = supercritical_net
        self.classifier = classifier
        self.epochs = 0
        self.best_loss = 1000000000
        self.name = "combonet"

    def forward(self, x):
        with torch.no_grad():
            water_type = self.classifier(x)

        


        '''result = torch.Tensor()
        for i in range(len(x)):
            if water_type[i] < 0.5:
                result = torch.cat((result, self.subnet(x[i]) ),0)
            else:
                result = torch.cat((result, self.supernet(x[i]) ),0)
        '''
        return result




import seaborn as sbs
import matplotlib.pyplot as plt
import pandas as pd

# All data
data = pd.read_csv('data/results.csv')

# Additional data work

data.reset_index()

model_names = ['I','II','III','IV']
the_names = [model_names[x] for x in data.index.values % 4]
data['Model'] = the_names
data['Hidden Units'] = [int(x) for x in data['Hidden Units']]
data['Network'] = data['Network'].replace("Chang", "Baseline")
for i in range(1,5):
    data['Name'] = data['Name'].replace("Chang{}".format(i), "Baseline{}".format(i))

for i in ['Mean', 'Median']:
    data[i] = data[i] * 100
    data = data.rename(columns={i: i +'[%]'})

# Data Sets
baseline = data.loc[data["Experiment"] == 'Baseline']
activation = data.loc[data["Experiment"] == 'Activation']
regularization = data.loc[data['Experiment'] == 'Regularization']
architecture = data.loc[data['Experiment'] == 'Architecture']
followup = data.loc[data['Experiment'] == 'Follow up']
final = data.loc[data['Experiment'] == 'Final']

# Baseline Results

if False:
    for trait in ['Mean', 'Median', 'Train Mean', 'Train Median']:
        baseline[trait] = baseline[trait] * 100

    new = []
    new_mse = []

    for i in range(4):
        for x in ['Mean', 'Median', 'Train Mean', 'Train Median']:
            data = {
                'Model' :  ['Model 1', 'Model 2', 'Model 3', 'Model 4'][i],
                'Value(%)': baseline.loc[i, x],
                'Type' : x
            }
            print(data)
            new.append(data)
        
        for x in ['MSE', 'Train MSE']:
            data = {
                'Model' :  ['Model 1', 'Model 2', 'Model 3', 'Model 4'][i],
                'Value': baseline.loc[i, x],
                'Type' : x
            }
            new_mse.append(data)

    new = pd.DataFrame(new)
    new_mse = pd.DataFrame(new_mse)

    barplot = sbs.barplot(x='Model', y='Value(%)', hue='Type', data=new)
    barplot.figure.savefig('sub_figures/baseline_percentages')
    plt.close()
    barplot = sbs.barplot(x='Model', y='Value', hue='Type', data=new_mse)
    barplot.figure.savefig('sub_figures/baseline_mse')
    plt.close()
# Activation Results
if False:
    activation = activation.drop(['Train Mean', 'Train Median', 'Train MSE'], axis=1)
    activation['Hidden Units'] = activation['Hidden Units'].replace(40.0, 'I')
    activation['Hidden Units'] = activation['Hidden Units'].replace(60.0, 'II')
    activation['Hidden Units'] = activation['Hidden Units'].replace(80.0, 'III')
    activation['Hidden Units'] = activation['Hidden Units'].replace(100.0, 'IV')
    activation['Mean'] = activation['Mean'] * 100
    activation['Median'] = activation['Median'] * 100

    activation = activation.rename(columns={'Network':'Activation Function',
    'Hidden Units':'Model'})

    act_mean = activation.pivot(index='Activation Function',
    columns='Model', values='Mean')

    act_med = activation.pivot(index='Activation Function',
    columns='Model', values='Median')

    act_mse = activation.pivot(index='Activation Function',
    columns='Model', values='MSE')

    print(act_mean)
    heatmap = sbs.heatmap(act_mean, vmax=1, vmin=0.6)
    heatmap.figure.savefig('sub_figures/act_mean')
    plt.close()
    heatmap = sbs.heatmap(act_med, vmax=1)
    heatmap.figure.savefig('sub_figures/act_med')
    plt.close()
    heatmap = sbs.heatmap(act_mse, vmax=20)
    heatmap.figure.savefig('sub_figures/act_mse')
    plt.close()
# Regularization Results
if False:
    for trait in ['Mean', 'Median', 'Train Mean', 'Train Median']:
        regularization[trait] = regularization[trait] * 100


    regularization['Ratio Mean'] = abs((regularization['Mean'] / regularization['Train Mean'])-1)

    regularization['Hidden Units'] = regularization['Hidden Units'].replace(40.0, 'I')
    regularization['Hidden Units'] = regularization['Hidden Units'].replace(60.0, 'II')
    regularization['Hidden Units'] = regularization['Hidden Units'].replace(80.0, 'III')
    regularization['Hidden Units'] = regularization['Hidden Units'].replace(100.0, 'IV')

    regularization = regularization.rename(columns={'Network':'Method',
        'Hidden Units':'Model'})

    reg_mean = regularization.pivot(index='Method',
    columns='Model', values='Mean')

    reg_med = regularization.pivot(index='Method',
    columns='Model', values='Median')

    reg_mse = regularization.pivot(index='Method',
    columns='Model', values='MSE')

    reg_ratio = regularization.pivot(index='Method',
    columns='Model', values='Ratio Mean')


    plt.subplots(figsize=(12,6))
    heatmap = sbs.heatmap(reg_mean, vmax=2)
    heatmap.figure.savefig('sub_figures/reg_mean')
    plt.close()

    plt.subplots(figsize=(12,6))
    heatmap = sbs.heatmap(reg_med,  vmax=1)
    heatmap.figure.savefig('sub_figures/reg_med')
    plt.close()

    plt.subplots(figsize=(12,6))
    heatmap = sbs.heatmap(reg_mse, vmax=40)
    heatmap.figure.savefig('sub_figures/reg_mse')
    plt.close()

    plt.subplots(figsize=(12,6))
    heatmap = sbs.heatmap(reg_ratio, vmax=1)
    heatmap.figure.savefig('sub_figures/reg_ratio')
    plt.close()
# Architecture Results
if False:
    one = architecture.loc[architecture['Layers']==1]
    two = architecture.loc[architecture['Layers']==2]
    three = architecture.loc[architecture['Layers']==3]
    four = architecture.loc[architecture['Layers']==4]

    # Generate One Layer Graphs

    for i in range(4):
        a = [one, two, three, four][i]

        fig = sbs.barplot(x='Hidden Units', y='Mean', hue='Model', data=a)
        plt.ylim(min(a['Mean'])-0.0002 , 0.9* max(a['Mean']))
        fig.figure.savefig('sub_figures/arc_{}_mean.png'.format(str(i)))
        plt.close()

        fig = sbs.barplot(x='Hidden Units', y='Median', hue='Model', data=a)
        plt.ylim(min(a['Median'])-0.0002 , 0.9* max(a['Median']))
        fig.figure.savefig('sub_figures/arc_{}_med.png'.format(str(i)))
        plt.close()

        fig = sbs.barplot(x='Hidden Units', y='MSE', hue='Model', data=a)
        plt.ylim(min(a['MSE'])-0.0002 , 0.9* max(a['MSE']))
        fig.figure.savefig('sub_figures/arc_{}_mse.png'.format(str(i)))
        plt.close()
# Follow up results
if False:
    followup
    fig = sbs.barplot(x='Layers', y='Mean', hue='Model', data=followup)
    plt.ylim(min(followup['Mean'])-0.0002 , 0.9* max(followup['Mean']))
    fig.figure.savefig('sub_figures/followup_mean.png')
    plt.close()

    fig = sbs.barplot(x='Layers', y='Median', hue='Model', data=followup)
    plt.ylim(min(followup['Median'])-0.0002 , 0.9* max(followup['Median']))
    fig.figure.savefig('sub_figures/followup_med.png')
    plt.close()

    fig = sbs.barplot(x='Layers', y='MSE', hue='Model', data=followup)
    plt.ylim(min(followup['MSE'])-0.0002 , 0.9* max(followup['MSE']))
    fig.figure.savefig('sub_figures/followup_mse.png')
    plt.close()

if True:

    baseline['Std[%]'] = [0.77, 0.52, 0.52, 0.53]
    final['Std[%]'] = [0.76, 0.85, 0.68, 0.71]

    compare = pd.DataFrame()
    compare = compare.append(baseline)
    compare = compare.append(final)
    compare.index = [0,2,4,6,1,3,5,7]
    compare = compare.reindex([0,1,2,3,4,5,6,7])

    print(compare)
    for i in ['Mean[%]', 'Median[%]', 'MSE', 'Std[%]']:
        fig = sbs.barplot(x="Network", y=i, hue="Model", data=compare)
        fig.figure.savefig('sub_figures/final_{}.png'.format(i[:-3]))
        plt.close()


    



    


\section{Literature Review}
\subsection{Supercritical Liquids}
\subsubsection{Basics}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{SupercriticalLiquids.png}
        \caption{By Rifleman 82 - Own work, CC0, https://commons.wikimedia.org/w/index.php?curid=74352891}
        \label{fig:final}
\end{figure}
\end{center}
In 1822, Baron Charles Cagniard de la Tour discovered supercritical liquids.
Supercritical fluids are the result of heating a fluid past it's triple point 
- normally resulting in a phase shift from a liquid to a gas. However, when the 
fluid is kept under enough pressure it can reach a critical point - past which it
becomes a supercritical fluid.

Supercritical fluids maintain some of the characteristics of a liquid and some 
of the characteristics of a gas - making them useful for many industries. 
For instance in the dyeing industry, technicians make use of supercritical carbon
dioxide to colour clothes. The ability of the supercritical liquids to dissolve 
a dye within itself, while maintaining the ability to diffuse like a gas allows 
technicians to dissolve the dye within the many different polymers used to create
clothes. Leading to a deeper and more even dye. The same technique can be used 
to impregnate many different materials.

\subsubsection{Applications to Supercritical Water Reactors}

In the application of Supercritical water reactors(SCWR), including the High Performance
Light Water Reactor, the value of supercritical water is in it's ability to avoid
the transition from liquid to gas without giving up many of the characteristics
that make it useful as both a conduit for heat energy transfer and a coolant.

Like non-critical water, supercritical water has heat transfer properties - 
that is it can transfer heat away from a nuclear reactor and move this to a 
heat engine, generating electricity. It can also be used as a coolant, preventing 
the nuclear reactor from overheating and triggering a runaway nuclear reaction 
that will (at a minimum) singe the surrounding province. 

However, unlike non-critical water, supercritical water doesn't transition from 
a liquid to a gas. This transition is dangerous in a nuclear reactors cooling 
system as it can lead to a 'boiling crisis', where a thin layer of vapour forms 
between the coolant and the cooling system. This leads to a degradation in the 
heat transfer properties of the coolant - leading to extra heat retained in 
the reactor, which then results in a crisis.

By removing the possibility of 'boiling crisis' we can drive more heat into the 
cooling system. According to Loewenberg et al. this can increase the possible 
thermal efficiency of the SCWR to 44\% \cite{LOEWENBERG2008}  - the implications of
which will lead to cheaper energy, less nuclear waste and a more competitive form
of clean energy.

\subsubsection{Modelling Problems}

To safely and correctly control a nuclear reactor - systems must be put into 
place to understand what is happening within the system and what scenarios that 
might lead to. This presents a problem for the application of supercritical water 
because understanding the heat transfer properties of the fluid is a complex task.

As presented in Loewenberg et al. "Forced convection heat transfer of Newtonian
liquids or gases [have] constant properties ... [which] can be described by 
dimensionless correlations" \cite{LOEWENBERG2008}[sec 2, par 1], this statement
assets that normal gases and liquids have properties that vary linearly - and 
we can use ratio's of those properties to predict properties we are interested in.

"In the neighborhood of the pseudo-critical line, however, the properties of 
supercritical water vary extremely non-linearly" \cite{LOEWENBERG2008}[sec 2, 
par 3]. This presents a problem for our modelling, as we cannot use all the 
correlations devised by physicists for non-supercritical liquids. In fact, to do
so is highly inaccurate.

Furthermore, "Dimensional analysis can easily lead to a
tremendous amount of dimensionless parameters to be taken into account.
Considering only linear Taylor series expansions, a total of 18 dimensionless 
parameters can be identified"\cite{LOEWENBERG2008}[sec 2, par 4]. This statement
asserts that to try and do the non-linear in a linear fashion, we would end up 
with such a large amount of parameters that the model would quickly become to 
complex to make progress with.

\subsection{Modeling Techniques}
Despite these problems, the merits of supercritical water have encouraged 
physicists and (more recently) data scientists alike to build models in the hopes
of accurately predicting the heat transfer properties of supercritical water.

\subsubsection{Heat Correlations}
As previously mentioned, physicists have been creating correlations between the 
properties of different fluids in different states for a long time.
Physicists have managed to derive two sets of equations that relate to the heat 
transfer properties of supercritical fluids. The first set are there to predict 
those properties, and generally they only do well within a narrow range of the 
entire problem space. The second set of equations can identify data points that 
shouldn't exist given a well built system that contains a fully developed flow.

These "heat deterioration correlations" can identify when a data point is under 
the effect of something odd (normally turbulence) and as such can be eliminated 
from a data set under the assumption that these data points would never be 
encountered in our SCWR.  

A paper by Cheng et al. provides a numerical analysis of the many different 
equations developed by physicists over the years. It provides an overview of 
the correlations developed \cite{CHENG2007240}[tab 1], their applicable ranges 
\cite{CHENG2007240}[tab 2], and the heat deterioration criterion each paper 
used.

An analysis of three heat correlations showed mean error percentages of 
22\%, 5.3\% and -5.1\% \cite{LOEWENBERG2008}[fig 4].

\subsubsection{Lookup Tables}
Loewenberg et al. proposes the method of using a lookup table to predict the 
wall temperatures of pipes containing supercritical water \cite{LOEWENBERG2008}
[tab 1]. 

A lookup table uses experimental data to form a table of references that a 
user can utilize to see what an output value should be given several 
different input values.

The advantage of a lookup table is that it's quick, and can be implemented 
very efficiently. It's also very accurate for the values that are represented 
in the lookup table.
The disadvantage of a lookup table is that it's a static method, it can only
give outputs for the input values that are coded within the table. 
Interpolation could be used to work with values between those static values 
given in the lookup table, but this will lead to greater inaccuracies the further 
away we move from the coded values.

The lookup table designed by Loewenberg et al. utilizes input variables of mass
flux, heat flux, pressure, tube diameter and bulk enthalpy. "As the flow
direction is restricted to vertical upward flow, heat transfer in supercritical 
water is only a function of [these input variables]"\cite{LOEWENBERG2008}[sec 3, 
par 2]. Wall temperature is given as the output variable, as the heat transfer
properties of supercritical water can be viewed as it's ability to transfer 
heat from the wall of it's pipe unto itself (and away to the heat exchanger).

The mean error of the lookup table is -1.7\% \cite{LOEWENBERG2008}[fig 4], far
better than the heat correlations.

\subsubsection{Direct Numerical Simulation}

One of the most advanced and accurate ways of predicting the heat transfer 
properties of supercritical water is through Direct Numerical Simulation (DNS).

DNS is used for computational fluid dynamics, that is the simulation of the 
movement of fluids within itself without using the simplification of a turbulence
model. This allows for an unprecedented level of accuracy in regards to the 
prediction of the properties of the fluid at the expense of a large amount 
of computational resources.

A paper by Chu et al. shows that datapoints can be generated for supercritical 
carbon dioxide using DNS, however "One case takes approximately 12,000 core 
hours on the \textit{Hanzel Hen} [Intel Xeon] computer" \cite{CHU2018}[sec 4].
The intense amount of computational resources required to accurate generate
cases and hence predict heat transfer properties for super critical liquids 
prohibits it's application to a real time system.

A final point is that the computational load of a direct numerical simulation 
can be reduced, by simplifying the model so that the smallest effects are
ignored. This type of modelling, known as "Large Eddy Simulation" is an 
interesting field of study that could hold the key to understanding the 
characteristics of supercritical water, as well as predicting the heat transfer 
properties of water to a high degree of accuracy and promptness.

\subsubsection{Aritifical Neural Networks}

Artificial Neural Networks (ANN's) are a computational model inspired by the 
human brain. They utilize the training and adaption of many simple functions 
to model increasing complex ones, and have shown promise in the field of 
supercritical heat transfer predictions.

A paper by Wanli Chang et al. shows that the application of simple one hidden
layers neural networks containing between 200 and 250 hidden units can achieve
a mean error percentage of between 0.22\% and 0.33\% when applied to a similar 
data set \cite{CHANG2017}[Tab 1]. These results far outperform both the heat 
correlations and the lookup table. Further more, the paper states that the 
prediction of one case only takes 'several milliseconds' \cite{CHANG2017}[sec 5,
par 2].

The paper by Chu et al. also applied neural networks to the data generated by 
their DNS, in an effort to reproduce the accuracy of the generation process 
while reducing the time required to generate the results. Their result was 
a neural network that could replicate results to a mean error of 0.08\% 
\cite{CHU2018}[Tab 2] while only taking 5.4ms to evaluate said result on a 
'regular computer' \cite{CHU2018}[sec 4, par 5].

It is clear from these results that artificial neural networks show a lot of 
promise, both in terms of accuracy and applicability to real time systems.

\subsection{Artificial Neural Networks}
\def\layersep{2.5cm}
\begin{center}
\begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep]
    \tikzstyle{every pin edge}=[<-,shorten <=1pt]
    \tikzstyle{neuron}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]
    \tikzstyle{input neuron}=[neuron, fill=green!50];
    \tikzstyle{output neuron}=[neuron, fill=red!50];
    \tikzstyle{hidden neuron}=[neuron, fill=blue!50];
    \tikzstyle{annot} = [text width=4em, text centered]

    % Draw the input layer nodes
    \foreach \name / \y in {1,...,4}
    % This is the same as writing \foreach \name / \y in {1/1,2/2,3/3,4/4}
        \node[input neuron, pin=left:Input \#\y] (I-\name) at (0,-\y) {};

    % Draw the hidden layer nodes
    \foreach \name / \y in {1,...,5}
        \path[yshift=0.5cm]
            node[hidden neuron] (H-\name) at (\layersep,-\y cm) {};

    % Draw the output layer node
    \node[output neuron,pin={[pin edge={->}]right:Output}, right of=H-3] (O) {};

    % Connect every node in the input layer with every node in the
    % hidden layer.
    \foreach \source in {1,...,4}
        \foreach \dest in {1,...,5}
            \path (I-\source) edge (H-\dest);

    % Connect every node in the hidden layer with the output layer
    \foreach \source in {1,...,5}
        \path (H-\source) edge (O);

    % Annotate the layers
    \node[annot,above of=H-1, node distance=1cm] (hl) {Hidden layer};
    \node[annot,left of=hl] {Input layer};
    \node[annot,right of=hl] {Output layer};
\end{tikzpicture}
\end{center}
\subsubsection{Basics}
Since we are using ANN's, it's important to define what a ANN is and all the 
hyperparameter that we as the architect of the ANN must use to effectively tune 
the model to the task at hand.

At the most base level, a neural network is a computation model comprised of 
a basic computational unit called a neuron. Each neuron has the ability to 
take in a set of inputs, perform a simple computation upon those inputs and 
deliver an output. The dynamic ability of the neuron is realized by it's
ability to modify a set of 'input weights', i.e. the importance it puts on the 
different inputs it receives. The equation for the output neuron is:
\begin{equation}
    output = f(\sum_{i=0}^N bias + w_i x_i)
\end{equation}

Where f is the activation function, the bias is a dynamic integer given to the
neuron, w is a vector containing all the input weights and x is a vector containing
all the inputs from connected neurons.

Neurons come in three types, input unit, hidden units and output units. Units 
are also organized into layers, where each unit of one layer has an input from 
every unit of the previous layer (excepting input units).
Input units form the first layer of every neural network, and this is where we 
feed our data into the neural network - ANN's will have one input unit for every 
input variable we feed into our network. Therefore our network will have five 
input units for the five variables we are feeding into our network (mass
flux, heat flux, pressure, tube diameter and bulk enthalpy). The network will 
also have an output unit for every output variable we have, because we are only
after one output variable (wall temperature), we will only be utilizing one output
unit in the networks final layer.
Finally, a network can have any number of hidden units in any number of hidden
layers - this is a hyperparameter for the network, i.e a parameter we must set 
by hand while building the network.

\subsection{Training and Gradient Descent}
Neural networks are useful when they become a model of a complex data set, and a 
key step in this process is the training of a neural network.
The process of training is the adjustment of the input weights and bias for every
hidden unit and output unit of our network. In adjusting these parameters, we 
hope to minimize a 'loss' function such that we can move the model closer to our 
goal. For instance, the loss function I will be using is Mean Squared Error (MSE),
because i hope to move the model to a state where there is the least Mean Squared 
Error between what the model is predicting, and what the actual results are.

This process can be broken down into several steps. 
First, we run a set of data points through our model and collect the set of 
predictions that the model gives us.
Second, we compare the set of predictions to the results we expected to see and 
calculate the error between the two.
Third, by using automatic differentiation, we can 'backpropergate' the error
from the output through to the input - this allows us to reason about how much 
error is the result of each neuron.
Finally, we can use a gradient descent algorithm to make the necessary changes to 
the parameters - so that we can improve the model.

This entire process happens iteratively, and continues until we decide to stop 
it. Stepping through our entire training dataset once is training the model for 
one 'epoch'.

\subsubsection{Hidden Units and Hidden Layers}
One hyperparameter we need to set before training is the amount of hidden layers 
our model will contain, and the amount of hidden units per layer.

The more hidden units we add to our network, the more complex the functions we 
can represent. We need to be sure that we have enough hidden units to accurately 
model the dataset we are using, however adding more hidden units than necessary
will increase the training time required for our model - and may increase the 
chance of overfitting.

As for hidden layers, the minimum we need is one hidden layer to do computation 
on a non linear dataset. A one hidden layer network can represent most models
[CITATION ENEDED], while a two hidden layer can be shown to represent all models.
However, the inclusion of more hidden layers than necessary can improve the 
training of neural networks by increasing the ratio of saddle points to local minima.
This is important, because saddle points are far easier to escape than local minima.
[Citation needed]. However, the same trade off exists for hidden layers, when we 
add more of them the training time of our model increases as well.

\subsubsection{Activation Functions}
The function that defines the computation that individual neurons perform on their
inputs is called an activation function.

Many different activation functions exist, because they are suited to different 
problem spaces. They have strengths and weaknesses in area's such as computational
resources required, how many neurons are required to represent more complex functions
and how well they can represent different models.

The base requirement for an activation function is that it must be non-linear (so
as to represent non-linear functions) and must be differentiable so that we can 
use automatic differentiation to train the neuron.

The industry standard for activation functions is the Rectified Linear Unit (ReLU),
which consists of a function that returns all positive outputs, and returns zero
for all negative output. This makes it non-linear, and very computationally
inexpensive. However, because there is no upper bound on what the function can 
return, it can run into gradient saturation problems (where the gradient becomes
too large to be stored by a computer). This and other issues have led to a family 
a ReLU activation functions, including the Exponential Linear Unit (ELU), a ReLU that does return a 
percentage of the negative input (LeakyReLU), a ReLU whose output is capped at 
a certain integer (ReLU6 for a cap at 6) and Scaled Exponential Linear Units (SELU).

Some other activation functions include the Sigmoid function and the TanH function,
function that have upper and lower bounds defined at [0, 1] and [-1, +1] 
respectively.

\subsubsection{Overfitting and Regularisation}

As with many machine learning techniques, neural networks can suffer from a 
propensity to stick far too closely to the training data and lose it's ability
to generalize - in a phenomenon known as 'overfitting'.

Overfitting impacts the accuracy of our model, and not normally because the 
model has been trained too little. In fact, within neural networks, overfitting
normally occurs when a model has obtained the overall picture of the dataset and 
then moves past it to learning the nitty gritty details (i.e. noise) of the 
dataset. Assuming the model has the capacity to do this (i.e. a nice reserve of 
hidden units) the model will do this with continued training.

Solutions to this issue are called 'Regularization' techniques. 
One technique is known as 'Early Stopping', where we stop the model training 
precisely at the point where the model starts overtraining. The decision on 
when to stop however is a delicate matter. A paper by 
Lutz Prechelt \cite{PRECHELT1998761} analyses and compares multiple stopping point
criteria which we can incorporate into our training algorithm to prevent
overtraining.

Another technique is 'Dropout', which involves randomly omitting a certain percentage
of all our hidden units during training. This prevents hidden units from working
together to learn the inane details of a dataset and forces them to stick to the 
big picture. A paper by Geoffrey E. et al. \cite{DROPOUT2012}
shows improvements to accuracy when using dropout layers in image recognition 
neural networks when compared to the same networks without dropout layers
\cite{DROPOUT2012}[fig. 2].

A third technique is called 'Batch Normalization'. Batch Normalization works 
by ensuring that the input to every layer has a mean of zero and is univariate.
A paper by Ioffe and Szegedy has show this to improve the performance and
generalization of a model \cite{BatchNormalisation}.

Finally L2 Regularisation, also known as weight decay, can prevent the weight's
in our network from becoming very large (unless there's a good reason for it). A
paper by Kroph and Hertz demonstrates the reduction in generalization error (i.e
overfitting) that weight decay can play \cite{krogh1992simple}[fig. 2]. 




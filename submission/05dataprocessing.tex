
\section{Date Processing}

It's important to explore, clean and standardize our data before we hand it over
to our neural network.
Exploring the data will allow us to identify subsets of data that we can use, 
either because they are not relevant to the problem space or before we don't have
enough of it to establish a pattern.
Cleaning the data will allow us to remove this data from the final dataset, as well
as deal with missing values and other problems that our raw data may present to us.
Finally, standardising the data set in a way that gives it a zero mean and makes 
it univariate will accelerate our learning. As explained in "Neural Network design
for engineering applications" - "Scaling of the inputs to the range [-1,+1]
greatly improves the learning speed, 
as these values fall in the region of the sigmoid transfer function 
where the output is most sensitive to variations of the input values"
\cite{rafiq2001neural}[sec 2.4, par 4]. Since we 
may be testing activation functions similar to the sigmoid function, and because 
we would like to avoid gradient saturation, scaling is an essential step for 
our data processing.

\subsection{Dataset}

The dataset used for this paper was collated from a set of experiments by 
researchers working with supercritical water. The dataset is not publicly
available.

The dataset comprises of 14324 datapoints with 35 different dimensions. 

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{initial.png}
        \caption{Median Error against our activation functions}
        \label{fig:initial}
\end{figure}
\end{center}

\subsection{Dimensionality Reduction}

First, we need to identify the dimensions that add information to our model and 
remove the dimensions that don't.
If we restrict our flow direction to a vertical upwards flow, "heat transfer in 
supercritical water is only a function of mass flux, heat flux, pressure, tube 
diameter and bulk enthaply" \cite{LOEWENBERG2008}[sec 3, par 2]. 

This allows us to reduce our datapoints to six dimensions (including the wall 
temperature).

\subsection{Data Range Reduction}
As we can see from , there are area's of the dataset that don't
cross into supercritical territory (i.e. 22MPa and 373 degrees Celsius).
We can also see area's of the dataset that form a very small percentage of the 
total amount of data points. Finally, we can see that there are outliers present 
in the dataset and not removing these will adversely affect our neural networks.

To resolve these effects, we can set restrict the range of our dataset:
\begin{enumerate}
    \item Mass Flux - Between 700 and 3500 $kg/m^2$
    \item Heat Flux - Between 300 and 1600 $kW/m^2$
    \item Pressure  - Between 22.5 and 25 MPa 
    \item Tube Diameter - Between 8 and 20mm
    \item Bulk Enthalpy - Between 1200 and 2700 $kj/kg$
\end{enumerate}

These ranges are the same as the ones used in "Heat Transfer Prediction of Supercritical Water
with Artificial Neural Networks" \cite{CHANG2017}[sec 3.2]

\subsection{Heat Deterioration Identification}

Since we are evaluating a model for the HPLWR, we want our model to only learn 
from data derived from water in a "fully developed flow" \cite{LOEWENBERG2008}
[sec 3, par 3]. Therefore, we need to apply a criterion against our dataset to 
identify datapoints derived from experiments under the influence of 'heat deterioration'.
One method of doing this is a function derived by Yamagata et al. \cite{yamagata1972forced}
\begin{equation}
    \textit{predicted heat flux} = 200 * \textit{Mass Flux}^{1.2}
\end{equation}
if the actual heat flux is larger than the predicted heat flux, then the datapoint
is affected by heat deterioration and removed from the dataset.

\subsection{Scaling and Normalization}
Data has been scaled and normalized to improve the learning rate.

\subsection{Final Dataset}

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{final.png}
        \caption{Median Error against our activation functions}
        \label{fig:final}
\end{figure}
\end{center}

This graph represents an overview of the final dataset that we are using,
the dataset contains 4996 datapoints each with 5 input variables and 1 output
variables. 
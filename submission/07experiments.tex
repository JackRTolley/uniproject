\section{Experiments}

\subsection{Architecture}
\subsubsection{Objective}
When it comes to hidden units and hidden layers, we want to find an architecture
that's expressive enough to capture the complexity of our model without adding
additional hidden units and hidden layers - as these increase the computational 
resources required to train and evaluate the model as well as increases the 
spare capacity that the model can use to overtrain. To do this, we will 
need to evaluate a large number of networks with different architectures.

Since the creation and training of neural networks is a computationally intensive
task, I will be evaluating networks using a Search-like method - to avoid wasting 
resources on networks that don't show any promise.
\subsubsection{Method}

The constant hyper parameters for these networks will be:
\begin{itemize}
    \item One input layer with five input units 
    \item One output layer with one output units
    \item We will be using the ReLU activation function
    \item Training will be done using the ADAM learning algorithm and will run for 5000 epochs
    \item The model used will be the one with the lowest validation error detected during the 5000 epoch training run
    \item Evaluation will be done against the validation set
\end{itemize}

The hyper parameters we will be modifying are:
\begin{itemize}
    \item The hidden layers in the network (1,2,3 or 4)
    \item The hidden units in the network (10 to 500)
\end{itemize}

We will start at a network using 200 hidden units and 1 hidden layer.
We will apply this network to our four different training and monitoring datasets.

We will then increase the number of hidden units per layer by 10 and check 
to see if the validation error on at least one model has improved. If so we will
repeatedly add hidden units until the validation error does not improve on any models.

When the validation error stops improving, we will then add one hidden layer - we
will choose a network that has a comparable amount of parameters to the best 
performing network from the previous set.

For instance, we could iterate from through networks with one hidden layer 
until we reach a network with 250 hidden units. If the performance stops 
increasing here we have a network with 1751 parameters - the comparable two 
hidden layer network is one with 30 hidden units, a network totaling 1271
parameters (a network with 40 hidden units would have 1961 parameters).

This should allow us to quickly find the optimal architecture.

\subsubsection{Results}

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_0_mean.png}
        \caption{Mean percentage error [\%] for 1 hidden layer networks}
        \label{fig:arc_0_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_1_mean.png}
        \caption{Mean percentage error [\%] for 2 hidden layer networks}
        \label{fig:arc_1_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_2_mean.png}
        \caption{Mean percentage error [\%] for 3 hidden layer networks}
        \label{fig:arc_2_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_3_mean.png}
        \caption{Mean percentage error [\%] for 4 hidden layer networks}
        \label{fig:arc_3_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_0_med.png}
        \caption{Median percentage error [\%] for 1 hidden layer networks}
        \label{fig:arc_0_med}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_1_med.png}
        \caption{Median percentage error [\%] for 2 hidden layer networks}
        \label{fig:arc_1_med}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_2_med.png}
        \caption{Median percentage error [\%] for 3 hidden layer networks}
        \label{fig:arc_2_med}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_3_med.png}
        \caption{Median percentage error [\%] for 4 hidden layer networks}
        \label{fig:arc_3_med}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_0_mse.png}
        \caption{Mean Squared Error for 1 hidden layer networks}
        \label{fig:arc_0_mse}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_1_mse.png}
        \caption{Mean Squared Error for 2 hidden layer networks}
        \label{fig:arc_1_mse}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_2_mse.png}
        \caption{Mean Squared Error for 3 hidden layer networks}
        \label{fig:arc_2_mse}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{arc_3_mse.png}
        \caption{Mean Squared Error for 4 hidden layer networks}
        \label{fig:arc_3_mse}
\end{figure}
\end{center}

These figures show the results of the experiment.
\subsubsection{Outcome}

During training, it become apparent that models over 33,000 parameters wouldn't
train effectively on the hardware I was using. Therefore we had to stop the training
of two layer models at 170 hidden units, three layers models at 120 hidden units 
and four layer models at 100 hidden units.

For one layer models, the model using 290 neurons performed well across all 
training sets - with the best performance for training sets II and IV. 
For two layer models, the model using 160 neurons performed consistently across 
the board - with the best performance for training sets III and IV.
For three layer models, the model using 110 neurons had the best performance 
for all training sets.
For four layer models the model using 90 neurons had the best performance 
across training set I, II and IV. With competitive performance of training set III.

\subsubsection{Follow Up}

During training, it became apparent that many models still had the potential to 
train further (the best model was close to the 5000 epoch mark).
Now we have our best models for each of the four different hidden layer
configurations, i want to test whether the difference between the models changes
when changed for a longer time period.

In this follow up experiment, I will be repeating the previous experiment with 
two changes.
\begin{enumerate}
    \item I will only be looking at our establish best model per hidden layer 
    configuration.
    \item I will be training for 25000 and the model tested will be the one that
    obtains the lowest error rate on it's monitoring set.
\end{enumerate}

\subsubsection{Results}

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{followup_mean.png}
        \caption{Mean error percentage [\%] for the followup networks}
        \label{fig:fup_mean}
    \end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{followup_med.png}
        \caption{Median percentage error [\%] for the followup networks}
        \label{fig:fup_med}
    \end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{followup_mse.png}
        \caption{Mean Squared Error for the followup networks}
        \label{fig:fup_mse}
    \end{figure}
\end{center}

\subsubsection{Outcome}

As we can see from the figures, the single hidden layer model with 290 hidden units
outperforms the other top models significantly. It is also the first choice when 
compared against all models in the experiment run. 

Therefore, we will be using a architecture of one hidden layer and 290 hidden units
for our final model.

\subsection{Activation Function}
\subsubsection{Objective}
Different activation functions perform better in different problem spaces. By 
analyzing and evaluating the performance of different activation functions in 
our neural network, the hope is that we can increase the performance of our 
network by a small but significant amount.

We will be testing seven activation functions in four different networks, to 
evaluate their applicability to the task at hand. Those seven functions will be 
ReLU, ELU, SELU, ReLU6, LeakyReLU, Sigmoid and TanH functions.

\subsubsection{Method}

The constant hyper parameters for our networks will be:
\begin{itemize}
    \item One input layer with five input units 
    \item One output layer with one output units
    \item Two hidden layers for every network.
    \item One network each with 40, 60, 80 and 100 hidden units.
    \item Training will be done using the ADAM learning algorithm
    \item Training will stop when the monitoring error rate stops improving
    \item Evaluation will be done against the validation set
\end{itemize}

The hyper parameters we will be modifying are:
\begin{itemize}
    \item The activation function for every hidden unit
\end{itemize}

This will be a straight forward test, where we apply the activation function 
to the four different networks, giving us 28 sets of results to compare and
contrast.
\subsubsection{Results}

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{act_mean.png}
        \caption{Mean Error (\%) for different activation functions}
        \label{fig:act_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{act_med.png}
        \caption{Median Error (\%) for different activation functions}
        \label{fig:act_med}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{act_mse.png}
        \caption{Mean Squared Error for different activation functions}
        \label{fig:act_mse}
\end{figure}
\end{center}

With a consistent performance across the three metrics, the TanH activation 
function clearly outperformed all others. The Mean Percentage Error was under
0.75\%, the Median Percentage Error was under 0.6\% and the MSE was under 10.4 
across all the models evaluated in this experiment.

\subsubsection{Outcome}

The results are clear, and we will be using the TanH activation function when 
we implement our final model. The TanH activation function also outperformed the
sigmoid function, so my hope is that in implementing the TanH activation function
i can make progress over the baseline model.

\subsection{Regularization techniques}
\subsubsection{Objective}

Regularization techniques are extremely important for neural networks, the 
prevention of overfitting is a must if one is to ensure that all the potential 
possible is being squeezed out of a neural network.
We have four different techniques to compare and contrast, and within those 
techniques we have different conditions and parameters that we can set and
observe the effect of.
\subsubsection{Method}

The constant hyper parameters for our networks will be:
\begin{itemize}
    \item One input layer with five input units 
    \item One output layer with one output units
    \item Two hidden layers for every network.
    \item One network each with 40, 60, 80 and 100 hidden units.
    \item We will be using the ReLU activation function
    \item Training will be done using the ADAM learning algorithm
    \item Training will stop at 25000 epochs or when the early stopping condition is met.
    \item Evaluation will be done against the validation set
\end{itemize}

The hyper parameters we will be modifying are:
\begin{itemize}
    \item The time when we stop training (Early Stopping).
    \item The inclusion of dropout layers in our network.
    \item The inclusion of batch normalization layers in our network.
    \item The inclusion of a weight decay variables on our learning algorithm.
\end{itemize}

With the exception of early stopping tests, all networks will be stopped once
the training set error rate stops decreasing.

We will be testing a total of 10 criteria for regularization in these four 
categories:
\begin{itemize}
    \item Early Stopping - Stop when validation error increases by 1 percent over minimum ($GL_1$)
    \item Early Stopping - Stop when validation error increases by 3 percent over minimum ($GL_3$)
    \item Early Stopping - Stop when validation error increases for 3 consecutive epochs ($UP_3$)
    \item Early Stopping - Stop when validation error increases for 4 consecutive epochs ($UP_3$)
    \item Dropout - 20\% Dropout layers, pre hidden layer
    \item Dropout - 50\% Dropout layers, pre hidden layer
    \item Dropout - 50\% Dropout layers, pre hidden layer and 20\% Dropout layer pre input layer
    \item Batch Normalization - The inclusion of batch normalization layers
    \item Weight Decay - set to 0.01
    \item Weight Decay - set to 0.02
    \item Weight Decay - set to 0.05
\end{itemize}

This will give us a set of 40 results to compare and contrast regularization techniques.

\subsubsection{Results}

\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{reg_mean.png}
        \caption{Validation mean percentage error [\%] for different regularisation methods}
        \label{fig:reg_mean}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{reg_med.png}
        \caption{Validation median percentage error [\%] for different regularisation methods}
        \label{fig:reg_median}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{reg_mse.png}
        \caption{Validation mean squared error for different regularisation methods}
        \label{fig:reg_mse}
\end{figure}
\end{center}
\begin{center}
    \begin{figure}[h!]
        \includegraphics[width=\linewidth]{reg_ratio.png}
        \caption{The absolute percentage difference between the validation mean 
        and the training mean for different regularisation methods. }
        \label{fig:reg_ratio}
\end{figure}
\end{center}

\subsubsection{Outcome}

We can see from figures \ref{fig:reg_mean}, \ref{fig:reg_median} and \ref{fig:reg_mse} that the batch normalization
models and the dropout models perform poorly in this experiment, with the early 
stopping technique middling and the weight decay technique performing the best.
The may be because the batch normalization and dropout techniques add significant
overhead to the models that we are training, and as such they are unable to 
reach their potential (despite being trained in real time for magnitudes longer 
than the early stopping or weight decay techniques).

However, we can see from \ref{fig:reg_ratio}, which represents the ration between 
the training loss and the validation loss (where 0 represents those values being 
equal), that the batch normalisation and dropout techniques outperform other 
techniques when the goal is to regularize the functions. With a dropout of 
20\% being a applied to the inputs and a dropout of 50\% being applied to the 
outputs maintaining the best regularisation.

In conclusion, we will be moving forward using the technique of weight decay, 
with our weight decay parameter being set to 0.01 - we have chosen this technique
because although it is among the worst at regularisation (though it does still
regularize) it maintains the best validation of the 10 techniques. 
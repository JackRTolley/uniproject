import torch
import torch.nn as nn
import torch.nn.functional as F
import pickle
import os.path
from copy import deepcopy
class Net(nn.Module):

    '''
    A class that represents a Linear Feed Forward Model, with a arbitary amount
    of nodes and hidden layers
    '''
    def __init__(self, name: str = None, layers: int=2, nodes:int = 64,
                 activation: str ='relu', dataset:str = None, optimizer:str=None,
                 hidden_dropout:float = 0.0, input_dropout = 0.0,
                 batch_normalisation: bool = False):
        super(Net, self).__init__()
        # Saving Parameters
        if name is None:
            self.name = "relu-{}-{}".format(layers, nodes)
        else:
            self.name = name
        
        self.dataset = dataset

        # Required Parameters
        self.epochs = 0
        self.best_loss = 1000000000
        self.optimizer = optimizer

        # Assign Activation function
        if activation == 'relu':
            self.activation = nn.ReLU()
        elif activation == 'elu':
            self.activation = nn.ELU()
        elif activation == 'leakyrelu':
            self.activation = nn.LeakyReLU()
        elif activation == 'relu6':
            self.activation = nn.ReLU6()
        elif activation == 'selu':
            self.activation = nn.SELU()
        elif activation == 'sig':
            self.activation = nn.Sigmoid()
        elif activation == 'tanh':
            self.activation = nn.Tanh()

        # Build model
        modules = []
        modules.append(nn.Dropout(input_dropout))
        modules.append(nn.Linear(5, nodes))
        if batch_normalisation:
            modules.append(nn.BatchNorm1d(nodes))
        modules.append(deepcopy(self.activation))
        modules.append(nn.Dropout(hidden_dropout))
        for i in range(1,layers):
            modules.append(nn.Linear(nodes, nodes))
            #if batch_normalisation:
            #    modules.append(nn.BatchNorm1d(nodes))
            modules.append(deepcopy(self.activation))
            modules.append(nn.Dropout(hidden_dropout))
        modules.append(nn.Linear(nodes, 1))
        self.model = nn.Sequential(*modules)

    def forward(self, x):
        return self.model(x)

    def save_model(self, optimizer):
        path = 'models/{0}.ckpt'.format(self.name)
        #print("Saving Model - {0}".format(self.name))
        torch.save({
            'name':self.name,
            'epoch':self.epochs,
            'dataset':self.dataset,
            'best_loss':self.best_loss,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
        }, path)

    def load_model(self, eval=False, optimizer=None):
        path = 'models/{0}.ckpt'.format(self.name)
        if os.path.isfile(path):
            print("Loading {0}".format(self.name))
            checkpoint = torch.load(path)
            self.name = checkpoint['name']
            self.dataset = checkpoint['dataset']
            self.epochs = checkpoint['epoch']
            self.best_loss = checkpoint['best_loss']
            self.model.load_state_dict(checkpoint['model_state_dict'])
            if not eval and optimizer is not None:
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                return True      
        else:
            print("No Model To Load")
            return False
    


        
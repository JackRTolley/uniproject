import argparse

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import os
import time
import numpy as np
import pandas as pd
import seaborn as sns

from torch.utils.data import TensorDataset, DataLoader

from sklearn import preprocessing
from model import Net

def report(epoch, total_loss):
    if epoch % args.report == 0:
        print("Epoch {} | Loss {}".format(epoch, total_loss))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('name', help='Name of model')
    parser.add_argument('epochs', help='How many epochs to train for', type=int)
    parser.add_argument('-i', '--improvement', help='Save on improvement',
                        action='store_true')
    parser.add_argument('-r', '--report',
                        help='How many epochs to report after',
                        type=int,
                        default=100)
    parser.add_argument('-eg', '--early_g',
        help='Implement Early Stopping Using GL parameter',
        type=int)
    parser.add_argument('-eu', '--early_u',
        help='Early Stopping Using U parameter',
        type=int)
    parser.add_argument('-s','--stopping',
        help='Dataset for early stopping')
    args = parser.parse_args()

    # Validate Arguments
    exists = os.path.isfile('models/{}.ckpt'.format(args.name))
    if not exists:
        raise ValueError("Model does not exist")

    if args.early_g or args.early_u:
        exists = os.path.isfile('data/{}.csv'.format(args.stopping))
        if not exists:
            raise ValueError("Early stopping dataset not available")

    if (args.early_g or args.early_u) and args.improvement:
        raise ValueError("Improvement and early stopping are mutually exclusive")
    
    if args.early_g and args.early_u:
        raise ValueError("Cannot use two early stopping criteria")
    # Load Optimiser + Model
    model = torch.load('models/{}.pt'.format(args.name))
    if model.optimizer == 'adam':
        optimizer = optim.Adam(model.parameters())
    elif model.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=0.001)
    model.load_model(optimizer=optimizer)

    # Load dataset
    data = np.loadtxt('data/{}.csv'.format(model.dataset), delimiter=",", skiprows=1)
    data_x = data[:, 1:6]
    data_y = data[:, 6]
    data_y = np.reshape(data_y,(-1,1))

    data_x = torch.Tensor(data_x)
    data_y = torch.Tensor(data_y)

    dataset = TensorDataset(data_x, data_y)
    dataset = DataLoader(dataset, num_workers=0, batch_size=16)

    # Load early dataset
    if args.stopping:
        early = np.loadtxt('data/{}.csv'.format(args.stopping), delimiter=",", skiprows=1)
        early_x = early[:, 1:6]
        early_y = early[:, 6]
        early_y = np.reshape(early_y,(-1,1))

        early_x = torch.Tensor(early_x)
        early_y = torch.Tensor(early_y)

        earlyset = TensorDataset(early_x, early_y)
        earlyset = DataLoader(earlyset, num_workers=0, batch_size=64)

    # Train
    criterion = nn.MSELoss()
    print("Training Started")
    
    # Save on improvement
    if args.improvement:
        min_val_loss = 999999999
        for epoch in range(args.epochs):
            total_loss = 0
            model.train()
            for data, labels in dataset:   
                # Training loop
                y_pred = model(data)
                loss = criterion(y_pred, labels)
                optimizer.zero_grad()    
                loss.backward()
                optimizer.step()
                # Save loss of batch
                total_loss += loss.item()

            if epoch % 100 == 0:
                val_loss = 0
                with torch.no_grad():
                    for data, labels in earlyset:
                        y_pred = model(data)
                        loss = criterion(y_pred, labels)
                        val_loss += loss.item()

                if val_loss < min_val_loss:
                    min_val_loss = val_loss
                    model.best_loss = total_loss
                    model.epochs = epoch
                    model.save_model(optimizer)

            # Report
            '''if total_loss < model.best_loss:
                model.best_loss = total_loss
                model.epochs = epoch
                model.save_model(optimizer)
                not_improved = 0
            report(epoch, total_loss)
            model.best_loss = total_loss
            model.epochs = epoch
            model.save_model(optimizer)'''

    elif args.early_g:     
        # Train
        min_val_loss = 999999999
        for epoch in range(args.epochs):
            total_loss = 0
            model.train()
            for data, labels in dataset:   
                # Training loop
                y_pred = model(data)
                loss = criterion(y_pred, labels)
                optimizer.zero_grad()    
                loss.backward()
                optimizer.step()
                # Save loss of batch
                total_loss += loss.item()
            report(epoch, total_loss)
            model.eval()
            val_loss = 0
            with torch.no_grad():
                for data, labels in earlyset:
                    y_pred = model(data)
                    loss = criterion(y_pred, labels)
                    val_loss += loss.item()

            if val_loss < min_val_loss:
                min_val_loss = val_loss

            elif ((val_loss/min_val_loss) - 1) * 100 > args.early_g:
                model.best_loss = total_loss
                model.epochs = epoch
                model.save_model(optimizer)
                break
            
            elif epoch % 1000 == 0:
                model.best_loss = total_loss
                model.epochs = epoch
                model.save_model(optimizer)

    
    elif args.early_u:

        last_val_loss = 9999999
        epochs_increased = 0
        for epoch in range(args.epochs):
            total_loss = 0
            model.train()
            for data, labels in dataset:   
                # Training loop
                y_pred = model(data)
                loss = criterion(y_pred, labels)
                optimizer.zero_grad()    
                loss.backward()
                optimizer.step()
                # Save loss of batch
                total_loss += loss.item()
            report(epoch, total_loss)
            model.eval()
            val_loss = 0
            with torch.no_grad():
                for data, labels in earlyset:
                    y_pred = model(data)
                    loss = criterion(y_pred, labels)
                    val_loss += loss.item()
            if val_loss > last_val_loss:     
                epochs_increased += 1
            else:
                epochs_increased = 0
            last_val_loss = val_loss
            if epochs_increased == args.early_u:
                model.epochs = epoch
                model.best_loss = total_loss
                model.save_model(optimizer)
                break
            if epoch % 1000 == 0:
                model.epochs = epoch
                model.best_loss = total_loss
                model.save_model(optimizer)

    # Basic Training
    else:
        for epoch in range(args.epochs):
            model.train()
            total_loss = 0
            for data, labels in dataset:
                y_pred = model(data)
                loss = criterion(y_pred, labels)
                optimizer.zero_grad()    
                loss.backward()
                optimizer.step()
                total_loss += loss.item()
            report(epoch, total_loss)
        model.epoch += epoch
        model.best_loss = total_loss
        model.save_model(optimizer)

    
    print("Training Finished")
    
            
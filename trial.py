import argparse
import subprocess
import os

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('layers', help='How Many Layers we will be using',
    type=int)
    parser.add_argument('units', help='How Many Units we will be using',
    type=int)
    args = parser.parse_args()

    # Create Networks

    name = 'L{}-{}'.format(args.layers, args.units)
    print("--- Creating Networks")
    subprocess.check_call(['python', 'create.py', '1'+name,
    str(args.layers), str(args.units), 'training1'])
    print("Network 1 Created")
    subprocess.check_call(['python', 'create.py', '2'+name,
    str(args.layers), str(args.units), 'training2'])
    print("Network 2 Created")
    subprocess.check_call(['python', 'create.py', '3'+name,
    str(args.layers), str(args.units), 'training3'])
    print("Network 3 Created")
    subprocess.check_call(['python', 'create.py', '4'+name,
    str(args.layers), str(args.units), 'training4'])
    print("Network 4 Created")

    # Create New training process
    print("--- Training Networks")
    subprocess.Popen(['python', 'train.py', '1'+name, '25000', '-i',
    '-s', 'monitor1'])
    print("Training model 1")
    subprocess.Popen(['python', 'train.py', '2'+name, '25000', '-i',
    '-s', 'monitor2'])
    print("Training model 2")
    subprocess.Popen(['python', 'train.py', '3'+name, '25000', '-i',
    '-s', 'monitor3'])
    print("Training model 3")
    subprocess.Popen(['python', 'train.py', '4'+name, '25000', '-i',
    '-s', 'monitor4'])
    print("Training model 4")

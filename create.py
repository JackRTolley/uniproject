import argparse
import os
import torch
import torch.optim as optim

from finalmodel import Net

if __name__ == "__main__":
    # Add Parsing options
    parser = argparse.ArgumentParser()
    parser.add_argument('name', help="Name of the model")
    parser.add_argument('layers', help="Number of hidden layers", type=int)
    parser.add_argument('nodes', help="Number of nodes per layers", type=int)
    parser.add_argument('dataset', help="The dataset to train the network from")
    parser.add_argument('--activation', help="Activation Function",
        choices=['relu','elu','leakyrelu','relu6','selu','sig','tanh'],
        default='relu')
    parser.add_argument('-o','--optimizer',
        help='The optimization algorithm for the model',
        choices=['adam','sgd'],
        default='adam')
    parser.add_argument('--indrop', help='Input Dropout rate',
        type=float,
        default=0.0)
    parser.add_argument('--hiddendrop', help='Hidden Dropout rate',
        type=float,
        default=0.0)
    parser.add_argument('-b','--batch', help='Activate Batch Normalisation',
    action='store_true')
    parser.add_argument('-w','--weight', help='Weight decay argument',
    type=float,
    default=0.0)
    args = parser.parse_args()
    
    # Check dataset is valid
    exists = os.path.isfile('data/{}.csv'.format(args.dataset))
    if not exists:
        raise ValueError('Dataset does not exist')
    # Create Neural Net
    '''model = Net(
        name=args.name,
        layers=args.layers,
        nodes=args.nodes,
        activation=args.activation,
        dataset=args.dataset,
        optimizer=args.optimizer,
        input_dropout=args.indrop,
        hidden_dropout=args.hiddendrop,
        batch_normalisation=args.batch
    )'''
    model = Net(
        name=args.name,
        dataset=args.dataset,
        optimizer=args.optimizer
    )
    # Create Optimizer
    if args.optimizer == 'adam':
        optimizer = optim.Adam(model.parameters(), weight_decay=args.weight)
    elif args.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=0.001)
    else:
        raise ValueError("Wrong Optimizer")
    # Save
    torch.save(model, "models/{}.pt".format(args.name))
    model.save_model(optimizer)
    

    

    
    
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import time
from heat_detoriation import Yamagata_1972, Styrikovich_1967
# SETTINGS
MAKE_PAIRWISE = False
SHOW_PLOTS = False
RESTRICT_VERTICAL_UPWARDS = True
RESTRICT_YAMAGATA = True    
RESTRICT_STYRIKOVICH = False 
RESTRICT_DATA_RANGE = True
REMOVE_OUTLIERS = False
NORMALIZE = True
LOW_DATA = False
CUSTOM_RESTRICTIONS = False

# IMPORT VARIABLES
data_path = "data/raw_data.csv"
output_path = "data/processed_data.csv"
fig_path = "figures/"

# HELPER FUNCTIONS
mtime = time.time()
def printl(msg):
    global mtime
    ntime = time.time()
    print("---" + msg + " | {0:.2f} secs".format(ntime - mtime) + " | Data len: " + str(len(data)))
    mtime = ntime
def make_pairwise(name):
    if MAKE_PAIRWISE:
        plot = sns.pairplot(data)
        plot.savefig(fig_path + name)
        printl("Pairwise Plot Created")

# *---------------------- START PROGRAM ------------------------------------

# ---------------------- DATA IMPORT  -------------------------------------
print("Importing Data")
data = pd.read_csv(data_path, header=1)
data = data.loc[:,["Mass Flux", "Heat Flux", "Pressure", "Diameter", "Bulk Enthalpy" , "Wall Temperature", "Angle"]]
all_test = data.loc[:,["Mass Flux", "Heat Flux", "Pressure", "Diameter", "Bulk Enthalpy" , "Wall Temperature"]]
all_test.to_csv('data/all_data.csv')
printl("Import Finished")
make_pairwise('initial')

# ---------------------- DATAHANDLING -------------------------------------
print("Filtering Data for Features and Responces")
if REMOVE_OUTLIERS:
    data = data.loc[data["Bulk Enthalpy"] > 150.0]
    data = data.loc[data["Wall Temperature"] > 100.0]
    printl("Outliers Removed")
    make_pairwise('outliers')
if RESTRICT_VERTICAL_UPWARDS:
    data = data.loc[data['Angle']==90]
    data = data.drop(columns=['Angle'])
    printl("Restricted to Vertical Upwards")
    make_pairwise('vertical')
if RESTRICT_YAMAGATA:
    data = data.loc[Yamagata_1972(data["Heat Flux"], data["Mass Flux"])]
    printl("Restricted to Yamagata")
    make_pairwise('heat_correlation')
elif RESTRICT_STYRIKOVICH:
    data = data.loc[Styrikovich_1967(data["Heat Flux"], data["Mass Flux"])]
    printl("Restricted to Styrikovich")
    make_pairwise('heat_correlation')
if RESTRICT_DATA_RANGE:
    data = data.loc[data["Bulk Enthalpy"] >= 1200]
    data = data.loc[data["Bulk Enthalpy"] <= 2700]
    printl("Bulk Enthalpy Restricted")
    data = data.loc[data["Diameter"] >= 8]
    data = data.loc[data["Diameter"] <= 20]
    printl("Diameter Restricted")
    data = data.loc[data["Pressure"] >= 22.5]
    data = data.loc[data["Pressure"] <= 25.0]
    printl("Pressure Restricted")
    data = data.loc[data["Heat Flux"] >= 300]
    data = data.loc[data["Heat Flux"] <= 1600]
    printl("Heat Flux Restricted")
    data = data.loc[data["Mass Flux"] >= 700]
    data = data.loc[data["Mass Flux"] <= 3500]
    printl("Mass Flux Restricted")
    make_pairwise('table_data_range')
if CUSTOM_RESTRICTIONS:
    data = data.loc[data['Pressure'] <= 27.0]
    printl("Pressure Restricted")
    data = data.loc[data["Diameter"] <= 20.1]
    printl("Diameter Restricted")
    data = data.loc[data['Wall Temperature'] >= 300]
    data = data.loc[data['Wall Temperature'] <= 550]
    printl("Wall Temperature Restricted")
    make_pairwise('custom_restrictions')
if LOW_DATA:
    data = data.loc[data["Diameter"].isin([10,15,20])]
    printl("Low Data Removed")
    make_pairwise('low_data')
if NORMALIZE:
    normal_labels = data['Wall Temperature']
    data.mean().to_csv('data/normalize_mean.csv', header=False)
    data.std().to_csv('data/normalize_std.csv', header=False)
    data = (data-data.mean())/data.std()
    data['Wall Temperature'] = normal_labels
    printl("Data Normalized")
    make_pairwise("normalized")
# -------------------- FIGURE CREATION -------------------------------------
# Create Seaborn pairwise graph
make_pairwise('final')
if SHOW_PLOTS:
    plt.show()

# --------------- Create Training And Test Sets -----------------------------
data = data.sample(frac=1)
data.to_csv('data/final_data.csv')


if False:
    training = data.sample(frac=0.95)
    testing = pd.concat([training, data]).drop_duplicates(keep=False)
    testing = testing.sample(frac=1)
    # Reset Index to zero
    training = training.reset_index()
    testing  = testing.reset_index()
    training = training.drop(columns=['index'])
    testing  = testing.drop(columns=['index'])

    data = training
    make_pairwise('training')
    data = testing
    make_pairwise('testing')

    # To CSV
    training.to_csv('data/training_data.csv')
    testing.to_csv('data/testing_data.csv')

if True: 
    # Validation Set
    validation = data.sample(n=198)
    data = pd.concat([validation, data]).drop_duplicates(keep=False)

    # Training Sets
    training1 = data.sample(n=1200)
    data = pd.concat([data, training1]).drop_duplicates(keep=False)

    training2 = data.sample(n=1200)
    data = pd.concat([data, training2]).drop_duplicates(keep=False)

    training3 = data.sample(n=1200)
    data = pd.concat([data, training3]).drop_duplicates(keep=False)

    training4 = data.sample(n=1200)
    data = pd.concat([data, training4]).drop_duplicates(keep=False)

    # Monitoring Sets
    monitor1 = training1.sample(frac=0.15)
    training1 = pd.concat([training1, monitor1]).drop_duplicates(keep=False)

    monitor2 = training2.sample(frac=0.15)
    training2 = pd.concat([training2, monitor1]).drop_duplicates(keep=False)

    monitor3 = training3.sample(frac=0.15)
    training3 = pd.concat([training3, monitor1]).drop_duplicates(keep=False)

    monitor4 = training4.sample(frac=0.15)
    training4 = pd.concat([training4, monitor1]).drop_duplicates(keep=False)

    #SaveSets 

    training1.to_csv('data/training1.csv')
    training2.to_csv('data/training2.csv')
    training3.to_csv('data/training3.csv')
    training4.to_csv('data/training4.csv')

    monitor1.to_csv('data/monitor1.csv')
    monitor2.to_csv('data/monitor2.csv')
    monitor3.to_csv('data/monitor3.csv')
    monitor4.to_csv('data/monitor4.csv')

    validation.to_csv('data/validation.csv')

print("Completed")
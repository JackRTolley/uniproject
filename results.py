import pandas as pd
import seaborn as sbs
import numpy as np

import torch
from finalmodel import MNet
from model import Net

import matplotlib.pyplot as plt


mean = np.array([1641.565170068026, 827.2143037214885, 23.682693077231146,
13.99676670668266, 1996.6136114445887])
std = np.array([784.1073996639345, 336.1027129606316, 0.8629828699901835, 
4.770518338412133, 283.71040828733265])

def normalize_input(data):

    return (data-mean)/data.std()

def load_data(file):

    data = np.loadtxt('data/{}.csv'.format(file), delimiter=",", skiprows=1)
    inputs = data[:, 1:6]
    labels = data[:, 6]
    labels = np.reshape(labels,(-1,1))

    inputs = torch.Tensor(inputs)
    labels = torch.Tensor(labels)

    return inputs, labels

def get_case(mass_flux, heat_flux, pressure, diameter):

    data = pd.read_csv('data/raw_data.csv', header=1)
    data = data.loc[:, ["Mass Flux", "Heat Flux", "Pressure", "Diameter", "Bulk Enthalpy" , "Wall Temperature", "Angle"]]
    data = data.loc[data['Angle']==90]
    data = data.drop(columns=['Angle'])
    data = data.loc[data['Mass Flux'] == mass_flux]
    data = data.loc[data['Heat Flux'] == heat_flux]
    data = data.loc[data['Pressure'] == pressure]
    data = data.loc[data['Diameter'] == diameter]

    y = data['Wall Temperature'].values
    data = data.drop(columns=['Wall Temperature'])
    x = data.values 
    return x,y 

if __name__ == '__main__':

    # Models
    Baseline1 = torch.load('models/Baseline1.pt')
    Baseline2 = torch.load('models/Baseline2.pt')
    Baseline3 = torch.load('models/Baseline3.pt')
    Baseline4 = torch.load('models/Baseline4.pt')
    BaselineModels = [Baseline1, Baseline2, Baseline3, Baseline4]
    Final1 = torch.load('models/Final1.pt')
    Final2 = torch.load('models/Final2.pt')
    Final3 = torch.load('models/Final3.pt')
    Final4 = torch.load('models/Final4.pt')
    FinalModels = [Final1, Final2, Final3, Final4]

    # Validation - graph 
    if False:
        x, y = load_data('validation')

        results = pd.DataFrame()
        models = ['I', 'II', 'III', 'IV']
        for i in range(4):
            new = pd.DataFrame()
            y_pred = BaselineModels[i](x).detach().numpy().flatten()
            new['Pred'] = y_pred
            new['Real'] = y.detach().numpy()
            new['Model'] = models[i]
            new['Type'] = 'Baseline'
            results = results.append(new)

        for i in range(4):
            new = pd.DataFrame()
            y_pred = FinalModels[i](x).detach().numpy().flatten()
            new['Pred'] = y_pred
            new['Real'] = y.detach().numpy()
            new['Model'] = models[i]
            new['Type'] = 'Final'
            results = results.append(new)
            
        print(results)
        fig = sbs.scatterplot(x='Real', y='Pred', hue='Type', 
        s=12, data=results)
        plt.plot([350,500],[350,500])
        fig.figure.savefig('sub_figures/final_overview.png')
        plt.close()

        base = results.loc[results['Type'] == 'Baseline']
        final = results.loc[results['Type'] == 'Final']

        fig = sbs.scatterplot(x='Real', y='Pred', 
        s=12, data=base)
        plt.plot([350,500],[350,500])
        fig.figure.savefig('sub_figures/final_base_overview.png')
        plt.close()
        fig = sbs.scatterplot(x='Real', y='Pred',
        s=12, data=final)
        plt.plot([350,500],[350,500])
        fig.figure.savefig('sub_figures/final_final_overview.png')
        plt.close()

    # Results Graphs
    if True:
    
        tests =[
            [700, 500, 22.5, 10],
            [1000,750,24,10],
            [1000, 700, 23.5, 10],
            [1500, 1500, 22.5, 10],
            [1500, 900, 24, 10],
            [2149.26, 787.5, 22.75, 9.42],
            [2250, 1000, 24, 10],
            [3500, 1400, 22.5, 20],
            [323, 384.88, 23.3, 8.0],
            [1260.0, 698.0, 24.5, 7.5]
        ]

        for index in range(0, 10):

            x, y = get_case(*tests[index])
            bulk = [ a[-1] for a in x]
            results = pd.DataFrame()
            results['Bulk Enthalpy'] = bulk
            results['Value'] = y 
            results['Type'] = 'Actual'
            results['Thing'] = 'Actual'
            norm_x = normalize_input(x)

            names = ['Baseline1', 'Baseline2', 'Baseline3', 'Baseline4']
            for i in range(4):
                pred = BaselineModels[i](torch.FloatTensor(norm_x)).detach().numpy()
                r = pd.DataFrame()
                r['Bulk Enthalpy'] = bulk
                r['Value'] = pred
                r['Type'] = names[i]
                r['Thing'] = 'Predicted'
                results = results.append(r)

            names = ['Final1', 'Final2', 'Final3', 'Final4']
            for i in range(4):
                pred = FinalModels[i](torch.FloatTensor(norm_x)).detach().numpy()
                r = pd.DataFrame()
                r['Bulk Enthalpy'] = bulk
                r['Value'] = pred
                r['Type'] = names[i]
                r['Thing'] = 'Predicted'
                results = results.append(r)

            fig = sbs.scatterplot(x='Bulk Enthalpy', y='Value', hue='Type',
            style='Thing',data=results)
            fig.figure.savefig('sub_figures/final_test{}.png'.format(index))
            plt.close()